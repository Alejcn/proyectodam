﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthPickup : MonoBehaviour
{
    public int vidaARecuperar = 100; //Variable para asignar la vida a recuperar
    private GameObject esteObjeto;
    public float velocidadRotacion = 180f;

    void Update(){
        movimientoHealthPickup();
    }

    private void OnTriggerEnter(Collider other){

        //Si este gameObject que tiene un collider, entra en contacto con el player recupera vida
        if(other.CompareTag("Player")){
            GameObject gameObject = other.gameObject; //Cogemos el player a traves de su collider
            PlayerController playerControllerScript = gameObject.GetComponent<PlayerController>(); //Cogemos el componente del script para poder llamar al metodo recuperacionDeVida()
            playerControllerScript.recuperacionDeVida(vidaARecuperar); //Le pasamos la vida a recuperar al personaje
            Destroy(this.gameObject);
        }
    }

    //Metodo para hacer que el objeto gire sobre si mismo y de efecto de animacion
    private void movimientoHealthPickup(){
        esteObjeto = this.gameObject;
        esteObjeto.transform.Rotate(Vector3.up * velocidadRotacion * Time.deltaTime);
    }
}
