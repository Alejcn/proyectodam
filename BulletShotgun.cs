﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletShotgun : MonoBehaviour
{
    public float velocidad = 8f; //Velocidad de la bala
    public float velocidadVida = 2f; //La velocidad de tiempo a la que la bala va a morir
    public int danyoBala = 40; //El daño que causará la bala
    private float tiempoVida; //variable a la que le asignaremos el tiempo de vida a la bala
    private GameObject player;
    public GameObject impactoBalaFX; //Variable donde colocaremos el efecto de bala en el inspector
    private bool stop = false; //Variable booleana que usaremos para parar el update() una vez dentro

    private void Start(){
        tiempoVida = velocidadVida; //Se inicializa la velocidad de vida en tiempo de vida
        player = GameObject.FindGameObjectWithTag("Player");
    }

    private void Update(){
        tiempoVida -= Time.deltaTime; //Se va actualizando el tiempo de vida en el tiempo del juego

        //Si el tiempo de vida llega a 0 se autodestruye la bala
        if(tiempoVida <= 0){
            Destroy(this.gameObject);
        }

        Debug.DrawRay(this.gameObject.transform.position, this.gameObject.transform.forward * 100f, Color.green); //Dibujamos una linea para ver a donde apunta el Raycast

        if(stop == false){
        RaycastHit hit; //creamos un objeto raycasthit para saber donde hemos disparado
          
            if(Physics.Raycast(this.gameObject.transform.position, this.gameObject.transform.forward, out hit)){ 

                if(hit.collider.tag == "Untagged" || hit.collider.tag == "Floor"){ //Si choca con algo con la etiqueta "Untagged" instancia el agujero de bala y se autodestruye a los 5 segundos
                    
                    GameObject hole = Instantiate(impactoBalaFX, hit.point, Quaternion.LookRotation(hit.normal));
                    Destroy(hole, 0.1f);
                    Destroy(this.gameObject);
                }
            }
        }
    }

    //Actualizamos la posicion de la bala por la velocidad y por el tiempo del juego
    private void FixedUpdate(){
        transform.position += transform.forward * velocidad * Time.fixedDeltaTime;
    }

    //Comprobamos con qué objeto choca la bala
    private void OnTriggerEnter(Collider other){
        if(other.CompareTag("Enemy")){
            GameObject g = other.gameObject; //Cogemos el enemigo a traves de su collider
            EnemyController enemyControllerScript = g.GetComponent<EnemyController>(); //Cogemos el componente del script para poder llamar al metodo de que ha sido disparado()
            enemyControllerScript.disparado(danyoBala); //al metodo de disparado() le pasamos el daño de la bala
            PlayerController playerControllerScript = player.GetComponent<PlayerController>();
            playerControllerScript.danyoRealizado();
            Destroy(this.gameObject); //Destruimos la bala
        }else{
            Destroy(this.gameObject);
        }
    }
}
