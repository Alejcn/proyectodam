﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    /*public float velocidad = 8f; //Velocidad de la bala
    public float velocidadVida = 2f; //La velocidad de tiempo a la que la bala va a morir
    public int danyoBala = 40; //El daño que causará la bala
    private float tiempoVida; //variable a la que le asignaremos el tiempo de vida a la bala
    private GameObject player;

    private void Start(){
        tiempoVida = velocidadVida; //Se inicializa la velocidad de vida en tiempo de vida
        player = GameObject.FindGameObjectWithTag("Player");
    }

    private void Update(){
        tiempoVida -= Time.deltaTime; //Se va actualizando el tiempo de vida en el tiempo del juego

        //Si el tiempo de vida llega a 0 se autodestruye la bala
        if(tiempoVida <= 0){
            Destroy(this.gameObject);
        }

        Debug.DrawRay(this.gameObject.transform.position, this.gameObject.transform.forward * 100f, Color.green); //Dibujamos una linea para ver a donde apunta el Raycast

    }

    //Actualizamos la posicion de la bala por la velocidad y por el tiempo del juego
    private void FixedUpdate(){
        transform.position += transform.forward * velocidad * Time.fixedDeltaTime;
    }

    //Comprobamos con qué objeto choca la bala
    private void OnTriggerEnter(Collider other){
        if(other.CompareTag("Enemy")){
            GameObject g = other.gameObject; //Cogemos el enemigo a traves de su collider
            EnemyController enemyControllerScript = g.GetComponent<EnemyController>(); //Cogemos el componente del script para poder llamar al metodo de que ha sido disparado()
            enemyControllerScript.disparado(danyoBala); //al metodo de disparado() le pasamos el daño de la bala
            PlayerController playerControllerScript = player.GetComponent<PlayerController>();
            playerControllerScript.danyoRealizado();
            Destroy(this.gameObject); //Destruimos la bala
        }else{
            Destroy(this.gameObject);
        }
    }*/
}
