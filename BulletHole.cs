﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletRaycast : MonoBehaviour
{
    public GameObject impactoBalaFX; //Variable donde colocaremos el efecto de bala en el inspector
    private bool stop = false; //Variable booleana que usaremos para parar el update() una vez dentro
    public int danyoBala = 40; //El daño que causará la bala
    private GameObject player;

    void Start(){
        player = GameObject.FindGameObjectWithTag("Player");
    }

    void Update(){
        
        if(stop == false){
        RaycastHit hit; //creamos un objeto raycasthit para saber donde hemos disparado
          
            if(Physics.Raycast(this.gameObject.transform.position, this.gameObject.transform.forward, out hit)){ 

                if(hit.collider.tag == "Untagged" || hit.collider.tag == "Floor"){ //Si choca con algo con la etiqueta "Untagged" instancia el agujero de bala y se autodestruye a los 5 segundos
                    
                    GameObject hole = Instantiate(impactoBalaFX, hit.point, Quaternion.LookRotation(hit.normal));
                    Destroy(hole, 0.1f);
                    Destroy(this.gameObject);
                }
                if(hit.collider.tag == "Enemy"){
                    //GameObject g = other.gameObject; //Cogemos el enemigo a traves de su collider
                    EnemyController enemyControllerScript = hit.collider.gameObject.GetComponent<EnemyController>();
                    //EnemyController enemyControllerScript = g.GetComponent<EnemyController>(); //Cogemos el componente del script para poder llamar al metodo de que ha sido disparado()
                    enemyControllerScript.disparado(danyoBala); //al metodo de disparado() le pasamos el daño de la bala
                    PlayerController playerControllerScript = player.GetComponent<PlayerController>();
                    playerControllerScript.danyoRealizado();
                    Destroy(this.gameObject); //Destruimos la bala
                }else{
                    Destroy(this.gameObject);
                }
                stop = true;
            }
        }
        Destroy(this.gameObject);
    }
}
