﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShopController : MonoBehaviour
{

    public GameObject[] weaponsListPrefabs;
    public Text[] weaponsPricesText;
    public int[] weaponPrices;
    public Button[] weaponsButtons;
    public Button[] buyWeaponsButtons;
    private int moneyAmountInt;
    public Text moneyAmount;

    private int isWeapon02Sold = 0;
    private int isWeapon03Sold = 0;
    
    // Start is called before the first frame update
    void Start()
    {
        for (int i = 1; i < weaponsButtons.Length; i++){
            weaponsButtons[i].interactable = false;
        } 

        int armaSlotUno = PlayerPrefs.GetInt("ArmaSlotUno");
        Debug.Log(armaSlotUno);
        /*if(armaSlotUno == 0){
            PlayerPrefs.SetInt("ArmaSlotUno", 1);
        }*/

    }

    // Update is called once per frame
    void Update()
    {
        moneyAmountInt = PlayerPrefs.GetInt("MoneyAmount");
        moneyAmount.text = moneyAmountInt.ToString() + " $";

        isWeapon02Sold = PlayerPrefs.GetInt("IsWeapon02Sold");
        if (moneyAmountInt >= weaponPrices[1] && isWeapon02Sold == 0){
            buyWeaponsButtons[1].interactable = true;
        }else{
            buyWeaponsButtons[1].interactable = false;
        }

        isWeapon03Sold = PlayerPrefs.GetInt("IsWeapon03Sold");
        if (moneyAmountInt >= weaponPrices[2] && isWeapon03Sold == 0){
            buyWeaponsButtons[2].interactable = true;
        }else{
            buyWeaponsButtons[2].interactable = false;
        }

        //-----------------

        if(isWeapon02Sold == 1){
            weaponsButtons[1].interactable = true;
            buyWeaponsButtons[1].gameObject.SetActive(false);
        }

        if(isWeapon03Sold == 1){
            weaponsButtons[2].interactable = true;
            buyWeaponsButtons[2].gameObject.SetActive(false);
        }        
    }

    public void buyWeapon02(){
        weaponsButtons[1].interactable = true;
        moneyAmountInt = PlayerPrefs.GetInt("MoneyAmount");
        moneyAmountInt -= weaponPrices[1];
        PlayerPrefs.SetInt("IsWeapon02Sold", 1);
        PlayerPrefs.SetInt("MoneyAmount", moneyAmountInt);
        weaponsPricesText[1].gameObject.SetActive(false);
        buyWeaponsButtons[1].gameObject.SetActive(false);
    }

    public void buyWeapon03(){
        weaponsButtons[2].interactable = true;
        moneyAmountInt = PlayerPrefs.GetInt("MoneyAmount");        
        moneyAmountInt -= weaponPrices[2];
        PlayerPrefs.SetInt("IsWeapon03Sold", 1);
        PlayerPrefs.SetInt("MoneyAmount", moneyAmountInt);
        weaponsPricesText[2].gameObject.SetActive(false);
        buyWeaponsButtons[2].gameObject.SetActive(false);
    }

    public void WeaponSelect(int index)
    {
        PlayerPrefs.SetInt("ArmaSlotUno", index);
    }
}
