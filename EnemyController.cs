﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyController : MonoBehaviour
{

    public Transform player; //Variable para asignarle el transform del player y que el enemigo nos mire/siga
    private GameObject[] playeer;

    public int vidaMaxima = 100; //Vida máxima que tendrá el personaje
    private int vidaActual; //Vida que tendrá el personaje durante la partida
    public float attackDistance = 1f; //Con esta variable modificamos la distancia desde donde nos atacan
    public float attackInterval = 2f; //Con esta le decimos el tiempo por cada ataque
    private float attackTime; //El tiempo que hay para atacar
    private float distanceToPlayer; //La distancia entre el enemigo y el player
    public GameObject armAttack; //Variable donde le asignaremos el objeto donde está el collider brazo con su daño hacia el player

    NavMeshAgent agent; //Creamos un NavMeshAgent que permite navegar por la escena al enemigo

    public float distanceToChase = 3f; //Distancia máxima que se acercará el enemigo
    public float chaseInterval = 2f; //intervalo de la actualizacion de posición del enemigo
    private float chaseTime; //El tiempo para volver a perseguir al player

    public GameObject[] objetosParaSpawnear;
    private bool muerto = false;

    // Start is called before the first frame update
    void Start(){
        agent = GetComponent<NavMeshAgent>(); //le asignamos al objeto agent el componente navMeshAgent del enemigo actual
        vidaActual = vidaMaxima;
        attackTime = attackInterval;
        chaseTime = chaseInterval;
        playeer = GameObject.FindGameObjectsWithTag("Player");
        //Transform transformPlayer = playeer[0];
    }

    // Update is called once per frame
    void Update(){        
        //Con esta línea conseguimos que el zombie ignore algunos colisionadores puestos por el mapa con los layers, para que pueda entrar a la escena y el player no salir
        Physics.IgnoreLayerCollision(10, 11);

        Vector3 rotacion = new Vector3(playeer[0].transform.position.x, transform.position.y, playeer[0].transform.position.z);//Sacamos la posición del player para saber donde tiene que rotar el enemigo
        transform.LookAt(rotacion); //Le decimos que rote a la posición del player

        distanceToPlayer = Vector3.Distance(transform.position, playeer[0].transform.position); //con esto podremos saber la distancia entre el enemigo y el personaje

        attackEnemy();
        chaseEnemy();
    }

    //Metodo que le quita vida al enemigo
    public void disparado(int damageRecibed){
        Debug.Log("me han quitado " + damageRecibed);
        vidaActual = vidaActual - damageRecibed; //Desde el script "Bullet.cs" recibimos el daño que recibirá el enemigo y se lo restamos a la vida actual

        Debug.Log("tengo de vida: " + vidaActual);
        //Si la vida llegase a 0 el enemigo moriria
        if(vidaActual <= 0 && !muerto){ 
            enemyDead();
            Debug.Log("HOLA A VER");
        }
    }

    //Metodo donde va el ataque del enemigo
    public void attackEnemy(){
        attackTime -= Time.deltaTime;
        //Este condicional entra si el tiempo de ataque ya ha pasado y si la distancia está cerca o lejos del personaje, si está cerca y el tiempo de ataque es cero entra (ataca)
        if(attackTime < 0 && distanceToPlayer < attackDistance){
            Animation animationArmAttack = armAttack.GetComponent<Animation>();//Recogemos la animación del ataque
            animationArmAttack.Play(); //Reproducimos la animación del ataque            
            attackTime = attackInterval; //le volvemos a asignar el tiempo de ataque
            
        }
    }

    //Metodo para la persecucion del enemigo hacia el player
    public void chaseEnemy(){
        chaseTime -= Time.deltaTime; //se le resta el tiempo del juego al tiempo de la persecución
        if(chaseTime < 0 && distanceToPlayer > distanceToChase){ //Si el tiempo de la persecución es menor que 0 Y la distancia al player es mayor que la distancia para perseguin entra
            agent.SetDestination(playeer[0].transform.position); //Le decimos al NavAgent el destino que es la posición del player
            agent.stoppingDistance = distanceToChase; //Le decimos al NavAgent donde tiene que pararse, en este caso la distancia entre el jugador que le hemos asignado
            chaseTime = chaseInterval; //reseteamos el chaseTime para que vuelva a perseguir una vez este llegue a 0 y entre a este metodo
        }
    }

    //Metodo para matar al zombie una vez se quede sin vida
    public void enemyDead(){
        muerto = true;
        int numeroZombiesActual = PlayerPrefs.GetInt("numeroZombiesActual");
        numeroZombiesActual = numeroZombiesActual -1;
        PlayerPrefs.SetInt("numeroZombiesActual", numeroZombiesActual);

        int zombiesAsesinados = PlayerPrefs.GetInt("zombiesAsesinados");
        zombiesAsesinados = zombiesAsesinados +1;
        PlayerPrefs.SetInt("zombiesAsesinados", zombiesAsesinados);

        int objeto = Random.Range(0, objetosParaSpawnear.Length);
        int randomNumber = Random.Range(0, 10);
        if(randomNumber == 5){
            Instantiate(objetosParaSpawnear[objeto], this.transform.position, this.transform.rotation); //Instanciamos la bala prefab y la guardamos en balaSpawn
        }
        Destroy(gameObject);
    }
}
