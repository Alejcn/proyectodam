﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour
{

    [Header("Canvas UI")]
    public GameObject mainMenuCanvas;
    public GameObject weaponSelectCanvas;
    public GameObject mapSelectCanvas;
    public GameObject loadingCanvas;
    public GameObject exitGameCanvas;
    public static int moneyAmountInt;
    public Text moneyAmount;


    // Start is called before the first frame update
    void Start(){
        weaponSelectCanvas.SetActive(false);
        mapSelectCanvas.SetActive(false);
        loadingCanvas.SetActive(false);
        exitGameCanvas.SetActive(false);
        mainMenuCanvas.SetActive(true);
        //PlayerPrefs.SetInt("MoneyAmount", 3000);
    }

    // Update is called once per frame
    void Update(){
        moneyAmount.text = moneyAmountInt.ToString() + " $";
        moneyAmountInt = PlayerPrefs.GetInt("MoneyAmount");
    }

    public void goMainMenuCanvas(){
        weaponSelectCanvas.SetActive(false);
        mapSelectCanvas.SetActive(false);
        loadingCanvas.SetActive(false);
        exitGameCanvas.SetActive(false);
        PlayerPrefs.Save();
        mainMenuCanvas.SetActive(true);
    }

    public void openMapSelectCanvas(){
        mapSelectCanvas.SetActive(true);
        mainMenuCanvas.SetActive(false);
    }

    public void openShopCanvas(){
        weaponSelectCanvas.SetActive(true);
        mainMenuCanvas.SetActive(false);
    }

    public void openExitCanvas(){
        exitGameCanvas.SetActive(true);
        mainMenuCanvas.SetActive(false);
    }

    public void exitGameButton(){
        Application.Quit();
        Debug.Log("Salimos del juego");
    }

    public void loadLevel(int levelIndex){
        mainMenuCanvas.SetActive(false);
        loadingCanvas.SetActive(true);
        Time.timeScale = 1;
        AudioListener.pause = false;
        GameObject player = GameObject.FindWithTag("Player");
        Destroy(player);
        SceneManager.LoadScene(levelIndex);
    }
}
