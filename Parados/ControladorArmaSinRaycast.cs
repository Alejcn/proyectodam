﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControladorArmaSinRaycast : MonoBehaviour
{
    public float velocidadDisparo = 20f; //Variable para cambiar la velocidad del disparo
    public float fuerzaDisparo = 80; //Variable para cambiar la fuerza del disparo

    public GameObject camaraArma; //Variable para asignar la camara en el inspector
    public ParticleSystem particulasDisparo; //Variable para poner un efecto de particulas en el inspector para cuando el arma dispara
    public GameObject impactoBalaFX; //Variable para poner efecto cuando choca la bala con algo que no sea un zombie
    //public GameObject impactoBalaZombieFX; //Variable para poner efecto cuando se le dispara al zombie
    public GameObject tipoMunicion;


    private float readyToFire;

    // Update is called once per frame
    void FixedUpdate(){
        if(Input.GetButton("Fire1") && Time.time >= readyToFire){ //le asignamos el disparo a la tecla Fire1
            readyToFire = Time.time + 1f/velocidadDisparo;
            disparo();
        }
    }

    //método que se ejecuta al disparar
    private void disparo(){
        particulasDisparo.Play(); //Ejecuta las particulas que le pusimos en la variable particulasDisparo
        RaycastHit hit; //Lanza un "rayo" y devuelve la información del objeto con el que chocó

        if(Physics.Raycast(camaraArma.transform.position, camaraArma.transform.forward, out hit)){
            if(hit.rigidbody != null)
                hit.rigidbody.AddForce(-hit.normal * fuerzaDisparo);
            Instantiate(impactoBalaFX, hit.point, Quaternion.LookRotation(hit.normal));
        }
    }
    
    /*private void disparo(){
        particulasDisparo.Play(); //Ejecuta las particulas que le pusimos en la variable particulasDisparo
        RaycastHit hit; //Lanza un "rayo" y devuelve la información del objeto con el que chocó

        if(Physics.Raycast(camaraArma.transform.position, camaraArma.transform.forward, out hit)){
            if(hit.rigidbody != null)
                hit.rigidbody.AddForce(-hit.normal * fuerzaDisparo);
            Instantiate(impactoBalaFX, hit.point, Quaternion.LookRotation(hit.normal));
        }
    }*/
}
