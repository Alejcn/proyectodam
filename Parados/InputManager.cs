﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

public class InputManager : MonoBehaviour
{
    private ControladorPersonaje controladorPersonaje;

    [HideInInspector] public float vertical;
    [HideInInspector] public float horizontal;
    [HideInInspector] public float xValue, yValue;

    void Start(){
        controladorPersonaje = GetComponent<ControladorPersonaje>();
    }

    void Update(){
        //Si se pulsa la tecla espacio, se ejecuta el salto
        if(Input.GetKeyDown(KeyCode.Space)){
            controladorPersonaje.Salto();
        }
    }

    void FixedUpdate()
    {
        vertical = Input.GetAxis("Vertical");
        horizontal = Input.GetAxis("Horizontal");
        xValue = CrossPlatformInputManager.GetAxis("Mouse Y");
        yValue = CrossPlatformInputManager.GetAxis("Mouse X");

    }
}
