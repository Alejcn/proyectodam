﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControladorPersonaje : MonoBehaviour
{
    private InputManager inputManager; //llama a la clase de InputManager
    private Rigidbody rigidbody; //crea un objeto rigidbody que se lo asignaremos al rigidbody de nuestro personaje
    private CapsuleCollider capsuleCollider;
    public GameObject camara; //crea un game object que se lo asignaremos a la camara en el inspector de Unity

    public float velocidadMovimiento = 1f; //La velocidad de movimiento que tendrá nuestro personaje, la podemos cambiar en el inspector de Unity
    public float fuerzaDeSalto = 200; //La fuerza que tendrá el salto
    [Range(0.1f, 20f)] public float sensibilidadRaton = 1f; //Crea un rango para aumentar o disminuir la sensibilidad del ratón

    public float saltoDrag, corriendoDrag, inactivoDrag;

    private void Start(){
        inputManager = GetComponent<InputManager>();
        rigidbody = GetComponent<Rigidbody>();
        capsuleCollider = GetComponent<CapsuleCollider>();
    }

    private void FixedUpdate(){
        //Comprobamos en el drag del rigidbody en el inspector, que cuando salta, corre, o está quieto, cambian a las variables que le asignamos
        rigidbody.drag = isGrounded() ? (inputManager.vertical != 0 || inputManager.horizontal != 0) ? corriendoDrag : inactivoDrag : saltoDrag;

        //Asignamos los movimientos al personaje
        rigidbody.AddForce(transform.forward * (inputManager.vertical * velocidadMovimiento));
        rigidbody.AddForce(transform.right * (inputManager.horizontal * velocidadMovimiento));

        //Asignamos la rotacion de la cámara
        transform.localRotation *= Quaternion.Euler(0f, inputManager.yValue * sensibilidadRaton, 0f);
        camara.transform.localRotation *= Quaternion.Euler(-inputManager.xValue * sensibilidadRaton, 0f, 0f);
    }

    //Con este método saltaremos gracias al añadir fuerza al transform.up
    public void Salto(){
        rigidbody.AddForce(transform.up * fuerzaDeSalto);
    }

    //método que nos sirve para saber si el jugador está tocando el suelo
    private bool isGrounded(){
        RaycastHit raycastHit;
        if(Physics.SphereCast(transform.position,capsuleCollider.radius * (1.0f - 0), Vector3.down, out raycastHit,
                ((capsuleCollider.height/2f) - capsuleCollider.radius) + 0.01f, Physics.AllLayers, QueryTriggerInteraction.Ignore)){
            return true;
        }else{
            return false;
        }
    }
}
