﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    public Transform camaraPersonaje; //Elemento para asignarle el transform de la camara en el inspector
    float verticalRaton; //hace referencia a la posicion vertical del ratón
    float horizontalRaton; //hace referencia a la posicion horizontal del ratón
    float rotacionCamara; //Es la rotación de la cámara inicial

    public float velocidadHorizontal; //La velocidad a la que se va a mover en el eje horizontal
    public float velocidadVertical; //La velocidad a la que se va a mover en el eje vertical

    public GameObject gameController;
    public CharacterController controlador; //Variable para asignarle al player donde está su controllador
    public float velocidadPersonaje = 12f; //La velocidad a la que se moverá el player, esta puede cambiarse desde el inspector de unity
    private float x; // Eje X en el que se mueve el personaje
    private float z; // Eje Z en el que se mueve el personaje
    Vector3 movimiento; //Variable para asignarle el movimiento
    
    Vector3 velocidadDeCaida; //La velocidad con el que el personaje cae cuando salta
    public float gravedad = -9; //Gravedad que tendrá
    private bool isGrounded = false; //Variable para saber si el personaje está tocando el suelo

    public float fuerzaDeSalto = 1f; //La fuerza de salto que tendrá, cuanto más alto más alto saltará
    private float valorDeSalto; //El resultado de la fuerza de salto por la gravedad

    public int vidaMaximaPlayer = 100; //Vida máxima que tendrá el jugador al iniciar la partida
    private int vidaActual; //Vida que se irá actualizando en la partida
    public Text vidaTexto; //Objeto para un componente de Texto, para saber la vida del personaje

    public Image imagenSangre; //Efecto de daño para la UI en forma de imagen, que se iniciará cuando el player reciba daño
    public float velocidadDesaparicionEfectoSangre = 0.5f; //El tiempo de duración del efecto de sangre una vez activado
    public GameObject imagenDanyoEnemigo; //Imagen que añadiremos desde el inspector para saber si le hemos dado al enemigo

    // Start is called before the first frame update
    void Start()
    {
        vidaActual = vidaMaximaPlayer;

        //Cursor.lockState = CursorLockMode.Locked; //Esto es solamente para que el cursor no aparezca cuando estemos jugando
        valorDeSalto = Mathf.Sqrt(fuerzaDeSalto * -2 * gravedad); //Saber la fuerza de salto que tendrá
    }

    // Update is called once per frame
    void Update()
    {
        //Condicional para que cuando el personaje está tocando el suelo la gravedad se vaya reiniciando
        if(isGrounded == true && velocidadDeCaida.y < 0){ 
            velocidadDeCaida.y = gravedad;
        }

        vidaTexto.text = vidaActual.ToString(); //Pasamos al Texto de la UI la vida actual para imprimirlo en pantalla

        movimientoCamara(); 
        movimientoPersonaje();
        saltoPersonaje();

        velocidadDeCaida.y += gravedad * Time.deltaTime; //Para que la gravedad esté presente
        controlador.Move(velocidadDeCaida * Time.deltaTime); //Para hacer que el player se mueva cuando caiga

        //startDesaparicionEfectoSangre();
    }

    //Método para el movimiento de la cámara con el ratón
    public void movimientoCamara(){
        horizontalRaton = Input.GetAxis("Mouse X") * velocidadHorizontal * Time.deltaTime;
        verticalRaton = Input.GetAxis("Mouse Y") * velocidadVertical * Time.deltaTime;

        rotacionCamara -= verticalRaton;
        rotacionCamara = Mathf.Clamp(rotacionCamara, -90f, 90f);
        transform.Rotate(0f, horizontalRaton, 0f);
        camaraPersonaje.localRotation = Quaternion.Euler(rotacionCamara, 0f, 0f);
    }

    //Método para el movimiento del personaje
    public void movimientoPersonaje(){
        x = Input.GetAxis("Horizontal");
        z = Input.GetAxis("Vertical");

        movimiento = transform.right * x + transform.forward * z;
        controlador.Move(movimiento * velocidadPersonaje * Time.deltaTime);
    }

    //Método para que el personaje salte
    public void saltoPersonaje(){
        /*Condicional para si pulsamos la tecla espacio y si isGrounded true entra, para que el jugador 
        no pueda saltar más mientras salta y para hacer que caiga el personaje*/
        if(Input.GetKeyDown(KeyCode.Space) && isGrounded){ 
            isGrounded = false;
            velocidadDeCaida.y = valorDeSalto;
            //danyoRealizado();
        }
    }

    //Metodos para visualizar que le hemos hecho daño al enemigo
    IEnumerator desactivarImagenDanyoEnemigo(){
        yield return new WaitForSeconds(0.1f);
        imagenDanyoEnemigo.SetActive(false);
    }
    public void danyoRealizado(){
        imagenDanyoEnemigo.SetActive(true);
        StartCoroutine(desactivarImagenDanyoEnemigo());
    }

    //Metodo para que el player reciba daño
    public void danyoRecibido(int damageRecibed){
        Debug.Log("me han quitado " + damageRecibed);
        vidaActual = vidaActual - damageRecibed; //Desde el script "ArmDamage.cs" recibimos el daño que recibirá el player y se lo restamos a la vida actual

        imagenSangre.color = new Color(imagenSangre.color.r, imagenSangre.color.g, imagenSangre.color.b, 0.30f);
        startDesaparicionEfectoSangre();

        Debug.Log("tengo de vida: " + vidaActual);
        if(vidaActual <= 0){ //Si la vida es 0 nuestro personaje morirá
            vidaActual = 0;
            playerDead();
        }
    }

    //Metodo por donde se le pasa vida a recuperar al player, desde el script Health.cs
    public void recuperacionDeVida(int vida){
        vidaActual += vida; //sumamos la vida recuperada a la actual
        if (vidaActual >= vidaMaximaPlayer){ //si la vidaActual es superior a la vida máxima, la vidaActual será igual a la vidaMaxima
            vidaActual = vidaMaximaPlayer;
        }
    }    

    //Metodo para hacer desaparecer el efecto sangre
    IEnumerator DesaparicionEfectoSangre(){
        float alpha = imagenSangre.color.a; //creamos una variable y le pasamos el alpha de la imagen actual
        while (alpha > 0){ //Hacemos que mientras el alpha sea mayor que 0 entre y se reste el mismo para ir haciendo el efecto de desaparicion
            alpha -= Time.deltaTime * velocidadDesaparicionEfectoSangre;
            imagenSangre.color = new Color(imagenSangre.color.r, imagenSangre.color.g, imagenSangre.color.b, alpha);
            yield return null;
        }
        yield return null;
    }

    //Metodo para inicializar el efecto de sangre
    public void startDesaparicionEfectoSangre(){
        StartCoroutine(DesaparicionEfectoSangre());
    }

    //Metodo para hacer que la partida acabe cuando el player muera
    private void playerDead(){
    GameController gameControllerScript = gameController.GetComponent<GameController>();
    gameControllerScript.gameOver();
   }

    //Método para que cuando el collider del controlador del personaje (componente) choque
    private void OnControllerColliderHit(ControllerColliderHit hit){
        if(hit.collider.CompareTag("Floor")){ //buscamos si con el objeto que ha chocado tiene la etiqueta Floor
            if(isGrounded == false){ //si no está en el suelo entra
                isGrounded = true; //conviertiéndo así a isGrounded is true para saber que el personaje está en el suelo
            }
        }
    }
}
