﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{
    public GameObject[] zombiesPrefab;
    public GameObject[] spawnPoinst;
    public GameObject[] weaponsPlayer;
    public GameObject spawnPlayer;
    public GameObject municionSpawn;
    public GameObject municionPrefab;

    public GameObject gameplayCanvas;
    public GameObject pauseCanvas;
    public GameObject gameOverCanvas;

    public Text numeroHordasText;
    public Text numeroZombiesActualText;
    private int numeroHordaActual;
    private int numeroZombiesActual;
    private int zombies = 8;
    private int puntuacionConseguida;
    private int dineroConseguido;
    public Text puntuacionConseguidaText;
    public Text zombiesMatadosText;
    public Text dineroConseguidoText;

    void Start(){
        int armaSlotUno = PlayerPrefs.GetInt("ArmaSlotUno");
        weaponsPlayer[armaSlotUno].SetActive(true);
        numeroHordaActual = 0;
        PlayerPrefs.SetInt("numeroHordasActual", numeroHordaActual);
        numeroZombiesActual = 0;
        PlayerPrefs.SetInt("numeroZombiesActual", numeroZombiesActual);
        PlayerPrefs.SetInt("zombiesAsesinados", 0);
    }

    // Update is called once per frame
    void Update(){
        numeroHordasText.text = "Ronda: " + numeroHordaActual.ToString();
        numeroZombiesActual = PlayerPrefs.GetInt("numeroZombiesActual");
        numeroZombiesActualText.text = "Zombies: " + numeroZombiesActual.ToString();
        
        if(numeroZombiesActual == 0){
            numeroHordaActual++;
            sistemaHordas();
        }

        if(Input.GetKeyDown("escape")){
            pauseGame();
        }
    }

    //Metodo para spawnear municion cuando nos quedemos a 0, se llama desde le weaponcontroller.cs
    public void spawnAmmo(){
        Instantiate(municionPrefab, municionSpawn.transform.position, municionSpawn.transform.rotation);
    }

    //Metodo para saber en que ronda nos encontramos y que pase a la siguiente
    public void sistemaHordas(){
        
        for (int i = 0; i < 100; i++)
        {
            if(numeroHordaActual == i){
                spawnZombie(zombies);
                zombies = zombies + 4;
            }
        }
    }

    //Metodo para spawnear un zombie random de los modelos que hallamos asignado, y de un spawnpoint random
    public void spawnZombie(int cantidadZombies){
        for (int i = 0; i < cantidadZombies; i++)
        {
            int zombieRandom = Random.Range(0, zombiesPrefab.Length);
            int spawnerRandom = Random.Range(0, spawnPoinst.Length);
            Instantiate(zombiesPrefab[zombieRandom], spawnPoinst[spawnerRandom].transform.position, spawnPoinst[spawnerRandom].transform.rotation);
            ++numeroZombiesActual; 
            PlayerPrefs.SetInt("numeroZombiesActual", numeroZombiesActual);
        }
    }

    //Metodo para que nos salga la pantalla de gameOver cuando nos maten, se llama desde la muerte del player
    public void gameOver(){
        Time.timeScale = 0;
        AudioListener.pause = true;
        gameOverCanvas.SetActive(true);
        gameplayCanvas.SetActive(false);
        int zombiesAsesinados = PlayerPrefs.GetInt("zombiesAsesinados");
        zombiesMatadosText.text = zombiesAsesinados.ToString();
        puntuacionConseguida = zombiesAsesinados * 5;
        puntuacionConseguidaText.text = puntuacionConseguida.ToString();
        dineroConseguido = puntuacionConseguida / 2;
        dineroConseguidoText.text = "+" + dineroConseguido.ToString() + "$";
        int moneyAmountInt = PlayerPrefs.GetInt("MoneyAmount");
        dineroConseguido = moneyAmountInt + dineroConseguido;
        PlayerPrefs.SetInt("MoneyAmount", dineroConseguido);
    }

    //Metodo para pausar el juego y que nos salga el menu de pausa
    public void pauseGame(){
        Time.timeScale = 0;
        AudioListener.pause = true;
        gameplayCanvas.SetActive(false);
        pauseCanvas.SetActive(true);
    }

    //Metodo para continuar el juego si estamos en pausa
    public void continueGame(){
        Time.timeScale = 1;
        AudioListener.pause = false;
        pauseCanvas.SetActive(false);
        gameplayCanvas.SetActive(true);
    }

    //Metodo que hará que se nos reinicie el nivel
    public void restartLevel(){
        Time.timeScale = 1;
        AudioListener.pause = false;
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    //Metodo para salir al menu principal
    public void exitLevel(){
        AudioListener.pause = false;
        Time.timeScale = 1;
        SceneManager.LoadScene("MenuPrincipal");
    }
}
