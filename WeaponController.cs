﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WeaponController : MonoBehaviour
{
    public float velocidadDisparo = 20f; //Variable para cambiar la velocidad del disparo
    public float precisionArmaEjeX = 0f; //Variable para asignar la precision en el eje X (derezha/izq)
    public float precisionArmaEjeY = 0f; //Variable para asignar la precision en el eje Y (altura)
    public GameObject camaraArma; //Variable para asignar la camara en el inspector
    public ParticleSystem particulasDisparo; //Variable para poner un efecto de particulas en el inspector para cuando el arma dispara
    public GameObject impactoBalaFX; //Variable para poner efecto cuando choca la bala con algo que no sea un zombie
    public GameObject balaPrefab; //Variable para añadir el prefab bala del arma
    public GameObject balaSpawner; //Variable para un gameobject que será de donde salga la bala
    public int numeroBalasPorDisparo = 1; //Variable para decidir cuantas balas saldrán al disparar una vez
    public int ammoPerMagazine = 0; //variable para asignarle la municion por cargador al arma
    public int ammoTotal = 124; //Variable para asignarle el total de balas que tendrá el arma
    private int ammoTotalStatic; //Variable para saber la municion total del arma siempre a pesar de no tener balas
    private int ammoActual; //Variable para saber cuantas balas hay en el cargador en el momento
    public float tiempoDeRecarga = 1f; //Variable para asignarle el tiempo con el que volverá a recargar el arma (cuanto más alto el num más rápido recarga)
    public Text textAmmo; //Texto para saber la cantidad de munición
    private bool hayMunicion = true; //Booleana para saber si hay munición
    public Image imagenRecargandoMunicion; //Imagen para hacer un efecto de recarga
    private float timeLeft; //Tiempo para la duracion del relleno de la imagen de recarga de municion
    private bool imagenRecargar = false; //Booleana para saber si la imagen
    public LayerMask ignoreLayerPlayer; //Creamos una capa que ignorará la capa que elijamos, en este caso la del jugador
    RaycastHit hit; //Creamos un Raycast para recoger la informacion del objeto donde vaya a impactar la bala
    private float readyToFire; //con esta variable y la velocidad de disparo conseguimos modificar el ratio del disparo
    private AudioSource audioSource;
    public AudioClip sonidoDisparo;
    public AudioClip sonidoSinMunicion;
    public AudioClip sonidoRecarga;
    public GameObject gameController;

    void Start(){
        ammoActual = ammoPerMagazine;
        ammoTotalStatic = ammoTotal;
        timeLeft = tiempoDeRecarga;
        audioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update(){
        //Dibujamos rayos en donde saldrán los raycast
        Debug.DrawRay(camaraArma.transform.position, camaraArma.transform.forward * 100f, Color.red);
        Debug.DrawRay(balaSpawner.gameObject.transform.position, this.gameObject.transform.forward * -100f, Color.blue);

        //Condicional para saber si hay municion y qué hacer
        if(hayMunicion){
            if(ammoTotal <= 0 && ammoActual <= 0){
                Debug.Log("SIN MUNICION");
                hayMunicion = false;
                GameController gameControllerScript = gameController.GetComponent<GameController>();
                gameControllerScript.spawnAmmo();
            }else if(ammoActual <= 0){
                recargaArma();
            }else if(Input.GetButton("Fire1") && Time.time >= readyToFire){ //le asignamos el disparo a la tecla Fire1
                readyToFire = Time.time + 1f/velocidadDisparo;
                disparo();
            }
            if (Input.GetKeyDown (KeyCode.R)){
                recargaArma();
            }
        }else if(Input.GetButton("Fire1") && Time.time >= readyToFire){
            readyToFire = Time.time + 1f/velocidadDisparo;
            Debug.Log("Aqui un sonido de cargador vacío");
            audioSource.PlayOneShot(sonidoSinMunicion);
        }

        //Le pasamos al textammo la cantidad de balas que quedan y el total
        textAmmo.text = ammoActual.ToString() + " / " + ammoTotal.ToString();

        //Condicional para saber si mostramos la imagen al recargar o no
        if(timeLeft > 0 && imagenRecargar == true && ammoActual == ammoPerMagazine){
                timeLeft -= Time.deltaTime;
                imagenRecargandoMunicion.fillAmount = timeLeft / tiempoDeRecarga;
        }else{
            imagenRecargar = false;
            timeLeft = tiempoDeRecarga;
        }
    }

    //método que se ejecuta al disparar
    private void disparo(){
        particulasDisparo.Play(); //Iniciamos las particulas
        audioSource.PlayOneShot(sonidoDisparo);
        ammoActual -= 1;
        //bucle for para saber cuantas balas disparar
        for(int i = 0; i < numeroBalasPorDisparo; i++){
            //Creamos un random para poder hacer un efecto de precision al disparar y que no sea siempre exacto
            Vector3 precisionArma = camaraArma.transform.TransformDirection(new Vector3(Random.Range(-precisionArmaEjeX, precisionArmaEjeX), Random.Range(-precisionArmaEjeY, precisionArmaEjeY), 1));

            GameObject balaSpawn = Instantiate(balaPrefab); //Instanciamos la bala prefab y la guardamos en balaSpawn
            balaSpawn.transform.position = balaSpawner.gameObject.transform.position; //le asignamos a la balaSpawn el transform de balaSpawner para que salga de ahi la bala
            if(Physics.Raycast(camaraArma.transform.position, precisionArma, out hit, Mathf.Infinity, ~ignoreLayerPlayer)){
                balaSpawn.transform.LookAt(hit.point);
            }else{
                Vector3 disparo = camaraArma.transform.position + precisionArma * 10f; //Le indicamos la dirección a la que disparará
                balaSpawn.transform.LookAt(disparo); //
            }
        }
    }

    //Metodo para recargar el arma que usemos
    private void recargaArma(){

        if(ammoTotal > 0 && ammoActual != ammoPerMagazine){  //Este if es para comprobar que no se haya gastado el cargador y no pueda seguir recargando
            readyToFire = Time.time + 1f * tiempoDeRecarga; //Con esta linea le asignamos el tiempo de recarga a la arma modificando la variable tiempoDeRecarga
            audioSource.PlayOneShot(sonidoRecarga);
            imagenRecargar = true;
            
        }
        
        if(ammoActual <= 0 && ammoTotal >= ammoPerMagazine){ //Este if comprueba si el cargador actual se ha vaciado, de ser un sí, se recargará entero
            ammoTotal -= ammoPerMagazine;
            ammoActual = ammoPerMagazine;
            audioSource.PlayOneShot(sonidoRecarga);
            imagenRecargar = true;
        }else if(ammoTotal <= 0){ //Este if comprueba si la munición total es 0 para que no pueda recargar el player
            Debug.Log("No se puede recargar");
        }else{ //Y finalmente si no se da ninguna de las anteriores condiciones, entra en un bucle contando las balas que le quedaban por meter en el cargador
            int ammoTemporal = 0;
            int ammoActualMasTotal = ammoActual + ammoTotal;
            if(ammoActualMasTotal < ammoPerMagazine){
                ammoActual = ammoActual + ammoTotal;
                ammoTotal = 0;
            }else{
                for (int i = ammoActual; i < ammoPerMagazine; i++){
                    ++ammoTemporal;
                    ++ammoActual;
                }
            }                
            ammoTotal -= ammoTemporal;
            if(ammoTotal < 0){  //Este if es para comprobar que no se haya gastado el cargador y no pueda seguir recargando la proxima vez
                ammoTotal = 0;
            }
        }
    }

    //Metodo que nos sirve para recuperar munción en la partida
    public void recuperacionDeMunicion(int municionARecuperar){
        ammoTotal = ammoTotalStatic; //Recuperamos la municion total
        ammoActual = ammoPerMagazine; //Recuperamos el cargador total
        hayMunicion = true;
    }
}
