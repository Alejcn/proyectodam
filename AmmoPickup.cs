﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AmmoPickup : MonoBehaviour
{
    public int municionARecuperar = 100; //Variable para asignar la municion a recuperar
    private GameObject esteObjeto;
    public float velocidadRotacion = 180f;

    void Update(){
        movimientoAmmoPickup();
    }

    private void OnTriggerEnter(Collider other){

        //Si este gameObject que tiene un collider, entra en contacto con el player recupera vida
        if(other.CompareTag("Player")){
            GameObject gameObject = other.gameObject; //Cogemos el player a traves de su collider
            WeaponController weaponControllerScript = gameObject.GetComponentInChildren<WeaponController>(); //Cogemos el componente del script para poder llamar al metodo recuperacionDeVida()
            weaponControllerScript.recuperacionDeMunicion(municionARecuperar); //Le pasamos la vida a recuperar al personaje
            Destroy(this.gameObject);
        }
    }

    //Metodo para hacer que el objeto gire sobre si mismo y de efecto de animacion
    private void movimientoAmmoPickup(){
        esteObjeto = this.gameObject;
        esteObjeto.transform.Rotate(Vector3.up * velocidadRotacion * Time.deltaTime);
    }
}
