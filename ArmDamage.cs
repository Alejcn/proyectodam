﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArmDamage : MonoBehaviour
{
    public int danyoArmZombie = 30; //daño que causara el objeto (en este caso el brazo) que choque con el Player
    public float attackInterval = 2f; //Con esta le decimos el tiempo por cada ataque
    private float attackTime; //Variable para saber el tiempo de ataque que le asignaremos con el intervalo

    void Start(){
        //le pasamos el tiempo de ataque a través del intervalo
        attackTime = attackInterval;
    }

    void Update(){
        //le restamos el tiempo que pasa por el tiempo de ataque para que este pueda llegar a 0
        attackTime -= Time.deltaTime;
    }

    private void OnTriggerEnter(Collider other){

        //Si este gameObject que tiene un collider, entra en contacto con el player Y además el tiempo de ataque (o sea que ha pasado el intervalo) es 0, entra a atacar al personaje
        if(other.CompareTag("Player") && attackTime < 0){
            GameObject g = other.gameObject; //Cogemos el player a traves de su collider
            PlayerController playerControllerScript = g.GetComponent<PlayerController>(); //Cogemos el componente del script para poder llamar al metodo de que ha sido disparado()
            playerControllerScript.danyoRecibido(danyoArmZombie); //Le pasamos el daño al player a través de su metodo danyo recibido
            attackTime = attackInterval; //volvemos a poner el araque en 2 segundos para que no ataque seguido el zombie
        }
    }
}
